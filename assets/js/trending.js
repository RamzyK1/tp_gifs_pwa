function setLoading(isLoading) {
  const loaderElement = document.getElementById("loader");
  const gifsElement = document.getElementById("gifs");
  if (isLoading) {
    loaderElement.style.display = null;
    gifsElement.style.display = "none";
  } else {
    loaderElement.style.display = "none";
    gifsElement.style.display = null;
  }
}

function addGIFToFavorite(event) {
  const likeButton = event.currentTarget;
  const gifId = likeButton.dataset.gifId;

  const gifElement = document.getElementById(gifId);

  const gifTitle = gifElement.querySelector("div h3").textContent;
  const gifVideoUrl = gifElement.querySelector("source").src;
  const gifImageUrl = gifElement.querySelector("img").src;

  const db = window.db;

  // TODO: 4a - Open IndexedDB's database
  db.open();

  // TODO: 4b - Save GIF data into IndexedDB's database
  db.gifs.add({
    id: gifId,
    title: gifTitle,
    imageUrl: gifImageUrl,
    videoUrl: gifVideoUrl,
  });
  // TODO: 4c - Put GIF media (image and video) into a cache named "gif-images"
  const gifsCacheName = "gif-images";
  const gifsFilesToCache = [gifVideoUrl, gifImageUrl];

  // TODO: 2b - On install, add app shell files to cache
  self.addEventListener("install", (event) => {
    console.log("Attempting to install gif to cache");
    event.waitUntil(
      caches.open(gifsCacheName).then((cache) => {
        return cache.addAll(gifsFilesToCache);
      })
    );
  });
  // Set button in 'liked' state (disable the button)
  likeButton.disabled = true;
}

function buildGIFCard(gifItem, isSaved) {
  // Create GIF Card element
  const newGifElement = document.createElement("article");
  newGifElement.classList.add("gif-card");
  newGifElement.id = gifItem.id;

  // Append GIF to card
  const gifImageElement = document.createElement("video");
  gifImageElement.autoplay = true;
  gifImageElement.loop = true;
  gifImageElement.muted = true;
  gifImageElement.setAttribute("playsinline", true);

  const videoSourceElement = document.createElement("source");
  videoSourceElement.src = gifItem.images.original.mp4;
  videoSourceElement.type = "video/mp4";
  gifImageElement.appendChild(videoSourceElement);

  const imageSourceElement = document.createElement("img");
  imageSourceElement.classList.add("lazyload");
  imageSourceElement.dataset.src = gifItem.images.original.webp;
  imageSourceElement.alt = `${gifItem.title} image`;
  gifImageElement.appendChild(imageSourceElement);

  newGifElement.appendChild(gifImageElement);

  // Append metadata to card
  const gifMetaContainerElement = document.createElement("div");
  newGifElement.appendChild(gifMetaContainerElement);

  // Append title to card metadata
  const gifTitleElement = document.createElement("h3");
  const gifTitleNode = document.createTextNode(gifItem.title || "No title");
  gifTitleElement.appendChild(gifTitleNode);
  gifMetaContainerElement.appendChild(gifTitleElement);

  // Append favorite button to card metadata
  const favButtonElement = document.createElement("button");
  favButtonElement.setAttribute("aria-label", `Save ${gifItem.title}`);
  favButtonElement.classList.add("button");
  favButtonElement.dataset.gifId = gifItem.id;
  favButtonElement.onclick = addGIFToFavorite;
  const favIconElement = document.createElement("i");
  favIconElement.classList.add("fas", "fa-heart");
  favButtonElement.appendChild(favIconElement);
  gifMetaContainerElement.appendChild(favButtonElement);

  // Disable button (set GIF as liked) if liked
  if (isSaved) {
    favButtonElement.disabled = true;
  }

  // Append GIF Card to DOM
  const articlesContainerElement = document.getElementById("gifs");
  articlesContainerElement.appendChild(newGifElement);
}

window.addEventListener("DOMContentLoaded", async function () {
  setLoading(true);

  const URL =
    "http://api.giphy.com/v1/gifs/trending?api_key=zSJbyeTtErP2sWJ0MYJckbICM1xqBshx&limiter=24";
  const urlOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  };

  try {
    fetch(URL, urlOptions)
      .then((res) => res.json())
      .then((json) => {
        const gifs = json.data;
        const db = window.db;

        gifs.forEach(async (gif) => {
          const gifsList = await db.gifs
            .where("id")
            .equalsIgnoreCase(gif.id)
            .toArray();
          const isSaved = gifsList.length == 0 ? false : true;
          buildGIFCard(gif, isSaved);
        });
      })
      .catch((e) => console.log(e));
  } catch (e) {
    console.log(e);
  } finally {
    setLoading(false);
  }
});
